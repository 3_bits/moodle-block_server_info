Server Information block
========================
By Fernando Acedo - 3-bits.com


About
-----
This block displays site information: CPU load, Memory load and Disk free space. 

It is part of Adaptable UI Set and needs theme Adaptable installed in moodle before to be used.

Available in English, Spanish and Catalan.


Requirements
------------
- Moodle 3.3
- Adaptable 1.4


Installation
------------
- Extract the zip file in your computer
- Copy 'site_info' folder into YOUR_MOODLE/blocks folder using FTP / SFTP
- As administrator go to Site Administration > Notifications
- Press "Upgrade Moodle database now" button and the block will be installed
- Go to the admin Dashboard
- Enable the custom page and select the block from "Add Block"
- Move the block to the top region in the Dashboard ideally in a "6+6" or "1" row type


License
--------
block_site_info is licensed under:
GPL v3 (GNU General Public License) - http://www.gnu.org/licenses
