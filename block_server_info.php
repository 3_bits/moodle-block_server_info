<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Block Server Information
 *
 * @package    block_server_info
 * @copyright  2017 Fernando Acedo (3-bits.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die();

class block_server_info extends block_base {
    public function init() {
        $this->title = get_string('pluginname', 'block_server_info');
    }

    public function has_config() {
        return false;
    }

    public function hide_header() {
        return false;
    }

    public function get_content() {
        global $DB, $OUTPUT;

        if ($this->content !== null) {
            return $this->content;
        }

        // CPU.
        $load = sys_getloadavg();
        $cpuload = $load[0] * 100;

        $alertlevel1 = '';
        if ($cpuload > 90) {
            $alertlevel1 = ' style="background-color: #ce090d"';
        } else if ($cpuload > 75) {
            $alertlevel1 = ' style="background-color: #ec8a16"';
        }

        // Memory.
        $free = shell_exec('free');
        $free = (string)trim($free);
        $freearr = explode("\n", $free);
        $mem = explode(" ", $freearr[1]);
        $mem = array_filter($mem);
        $mem = array_merge($mem);
        $memoryusage = $mem[2] / $mem[1] * 100;
        $memoryload = round($memoryusage, 0);

        $alertlevel2 = '';
        if ($memoryload > 90) {
            $alertlevel2 = ' style="background-color: #ce090d"';
        } else if ($memoryload > 75) {
            $alertlevel2 = ' style="background-color: #ec8a16"';
        }

        // Disk total / free.
        $disktotal = disk_total_space("/");
        $diskfree  = disk_free_space("/");
        $disk = round(($diskfree / $disktotal) * 100, 0);

        $alertlevel3 = '';
        if ($disk < 15) {
            $alertlevel3 = ' style="background-color: #ce090d"';
        } else if ($disk < 25) {
            $alertlevel3 = ' style="background-color: #ec8a16"';
        }

        // Show data ********************************************************************.
        $this->content = new stdClass;
        $this->content->text  = '<div class="row-fluid">';

        // CPU.
        $this->content->text .= '<div class="span4">';
        $this->content->text .= '<div class="server-info-container"'.$alertlevel1.'>';
        $this->content->text .= '<span class="site-info-value">'.$cpuload.'%</span>';
        $this->content->text .= '<span class="site-info-label">'.get_string('cpu', 'block_server_info').'</span>';
        $this->content->text .= '</div>';
        $this->content->text .= '</div>';

        // Memory.
        $this->content->text .= '<div class="span4">';
        $this->content->text .= '<div class="server-info-container"'.$alertlevel2.'>';
        $this->content->text .= '<span class="site-info-value">'.$memoryload.'%</span>';
        $this->content->text .= '<span class="site-info-label">'.get_string('memory', 'block_server_info').'</span>';
        $this->content->text .= '</div>';
        $this->content->text .= '</div>';

        // Disk.
        $this->content->text .= '<div class="span4">';
        $this->content->text .= '<div class="server-info-container"'.$alertlevel3.'>';
        $this->content->text .= '<span class="site-info-value">'.$disk.'%</span>';
        $this->content->text .= '<span class="site-info-label">'.get_string('disk', 'block_server_info').'</span>';
        $this->content->text .= '</div>';
        $this->content->text .= '</div>';

        // End show data ****************************************************************.
        $this->content->text .= '</div>';

        return $this->content;
    }
}
